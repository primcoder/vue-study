export default {
    state: {
        linksList: [
            {
                id: 1,
                title: 'Home',
                url: '/'
            },
            {
                id: 2,
                title: 'Add note',
                url: '/addNote'
            }
        ],
    },
    getters: {
        getLinksList (state) {
            return state.linksList;
        }
    }
}
