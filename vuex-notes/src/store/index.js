import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import note from './note'
import links from './links'

export default new Vuex.Store({
    modules: {
        note, links
    },

    state: {

    },
    mutations: {

    },
    actions: {

    },
    getters: {

    }
})