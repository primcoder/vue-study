export default {
    state: {
        noteList: [
            {
                id: 1,
                title: 'First note',
                descr: 'Description for the first note',
                date: new Date(Date.now()).toLocaleString(),
                priority: 'standard'
            },
            {
                id: 2,
                title: 'Second note',
                descr: 'Description for the second note',
                date: new Date(Date.now()).toLocaleString(),
                priority: 'important'
            },
            {
                id: 3,
                title: 'Third note',
                descr: 'Description for the third note',
                date: new Date(Date.now()).toLocaleString(),
                priority: 'veryImportant'
            }
        ],
    },
    mutations: {
        addNote (state, payload) {
            state.noteList.push(payload)
        },
        deleteNote (state, payload) {
            state.noteList.splice(payload, 1)
        },
        editNote (state, payload) {
            state.noteList[payload.i] = payload.note
        }
    },
    actions: {
        addNote ({commit}, payload) {
            commit('addNote', payload)
        },
        deleteNote ({commit}, payload) {
            commit('deleteNote', payload)
        },
        editNote ({commit}, payload) {
            commit('editNote', payload)
        }
    },
    getters: {
        getNotesList (state) {
            return state.noteList;
        },
        getNote: (state) =>(id) => {
            return state.noteList.find(note => note.id === +id)
        }
    }
}
