import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Home from '@/pages/Home'
import NotFound from '@/pages/404'
import Note from '@/pages/Note'
import AddNote from '@/pages/AddNote'

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/addNote',
            name: 'addNote',
            component: AddNote
        },
        {
            path: '/notes/:id',
            name: 'note',
            component: Note
        },
        {
            path: '*',
            name: 'notFound',
            component: NotFound
        }
    ]
})