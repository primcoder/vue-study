export default {
    state: {
        shopList: [
            {
                id: 1,
                title: 'Nike Red',
                descr: 'Lorem Классны кросовки',
                img: require('../assets/img/1.png'),
                gallery: [
                    { title: 'Nike1', img: require('../assets/img/1.png')},
                    { title: 'Nike2', img: require('../assets/img/2.png')},
                    { title: 'Nike3', img: require('../assets/img/3.png')},
                ]
            },
            {
                id: 2,
                title: 'Nike Red2',
                descr: 'Lorem Классны кросовки',
                img: require('../assets/img/4.png'),
                gallery: [
                    { title: 'Nike1', img: require('../assets/img/4.png')},
                    { title: 'Nike2', img: require('../assets/img/5.png')},
                    { title: 'Nike3', img: require('../assets/img/6.png')},
                ]
            },
            {
                id: 3,
                title: 'Nike Red3',
                descr: 'Lorem Классны кросовки',
                img: require('../assets/img/7.png'),
                gallery: [
                    { title: 'Nike1', img: require('../assets/img/7.png')},
                    { title: 'Nike2', img: require('../assets/img/8.png')},
                    { title: 'Nike3', img: require('../assets/img/9.png')},
                ]
            },
            {
                id: 4,
                title: 'Nike Red4',
                descr: 'Lorem Классны кросовки',
                img: require('../assets/img/10.png'),
                gallery: [
                    { title: 'Nike1', img: require('../assets/img/11.png')},
                    { title: 'Nike2', img: require('../assets/img/12.png')},
                ]
            }
        ]
    },
    getters: {
        getShopList (state) {
            return state.shopList;
        },
        getProduct: (state) =>(id) => {
            return state.shopList.find(product => product.id === +id)
        }
    }
}