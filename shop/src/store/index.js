import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import shop from './shop'

export default new Vuex.Store({
    modules: {
        shop
    },

    state: {
        message: 'hello vuex'
    },
    mutations: {
        setMessage (state, payload) { //payload - если что то надо передать
            state.message = payload
        }
    },
    actions: {
        setMessage ({commit}, payload) {
            commit('setMessage', payload)
        }
    },
    getters: {
        getMessage (state) {
            return state.message
        }
    }
})