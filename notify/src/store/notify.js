import loadMore from '../assets/js/loadMore.js'
import axios from 'axios'

export default {
    state: {
        messages: {
            'all' : [],
            'main' : []
        }
    },
    mutations: {
        setMessage (state, payload) {
            state.messages.all = payload
        },
        setMessageMain (state, payload) {
            state.messages.main = payload
        },
        loadMessages (state, payload) {
            state.messages.main = [...state.messages.main, ...payload]
        }
    },
    actions: {
        setMessage({commit}, payload) {
            commit('setMessage', payload)
        },
        setMessageMain({commit}, payload) {
            commit('setMessageMain', payload)
        },
        loadMessages ({commit, getters}) {
            let res = getters.getMessageFilter
            commit('loadMessages', loadMore(res, 2))
        },
        loadNotifyLazy ({commit, dispatch}) {
            commit('setLoading', true)
            setTimeout(() => {
                dispatch('loadNotify')
            }, 1800)
        },
        loadNotify ({commit}) {
            commit('setLoading', true)
            axios
                .get('https://tocode.ru/static/c/vue-pro/notifyApi.php')
                .then(response => {
                    let res = response.data.notify,
                        messages = [],
                        messagesMain = [];

                    for (let i = 0; i < res.length; i++) {
                        if(res[i].main) messagesMain.push(res[i])
                        else messages.push(res[i])
                    }

                    commit('setMessage', messages)
                    commit('setMessageMain', messagesMain)
                })
                .catch(error => {
                    console.log(error)
                    commit('setError', "Error: Network Error")
                })
                .finally(() => {commit('setLoading', false)})
        }
    },
    getters: {
        getMessage (state) {
            return state.messages.all
        },
        getMessageFilter (state) {
            return state.messages.all.filter(mes => {
                return mes.main === false
            })
        },
        getMessageMain (state) {
            return state.messages.main
        }
    }
}